httperf (0.9.0-9) unstable; urgency=medium

  * debian/control: add salsa URLs
  * debian/control: bump standard to 4.2.2 (no changes)
  * debian/control: use dh11
  * debian/control: remove dependency of dh-autoreconf

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 05 Sep 2018 19:12:44 +0200

httperf (0.9.0-8) unstable; urgency=medium

  * move to unstable
  * debian/control: bump standard to 4.1.0 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 09 Sep 2017 19:12:44 +0200

httperf (0.9.0-7) experimental; urgency=medium

  * new maintainer (Closes: #726548)
  * debian/control: use dh10
  * debian/control: use secure VCS URLs
  * let it compile with SSL 1.1.0 (Closes: #851074)

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 23 Mar 2017 18:12:44 +0100

httperf (0.9.0-6) unstable; urgency=medium

  * QA upload.
  * Fix FTBFS with openssl 1.1.0. Thanks to Adrian Bunk. (Closes: #828343)
  * Bump Standards-Version to 3.9.8.

 -- Chris Lamb <lamby@debian.org>  Sat, 12 Nov 2016 14:12:44 +0000

httperf (0.9.0-5) unstable; urgency=medium

  * QA upload.
  * Added patch for reproducible building.

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 15 Mar 2016 19:45:38 +0100

httperf (0.9.0-4) unstable; urgency=medium

  * QA upload.
  * New upstream homepage.
  * Migrations:
      - debian/copyright to 1.0 format.
      - debian/rules to new (reduced) format.
      - DebSrc to 3.0.
      - DH level to 9.
      - Using dh-autoreconf now.
  * debian/control:
      - Bumped Standards-Version to 3.9.7.
      - Improved the long description.
      - Removed the unnecessary dependency quilt from Build-Depends field.
      - Updated the Vcs-* fields.
  * debian/copyright: updated all information and licenses.
  * debian/dirs: unnecessary. Removed.
  * debian/manpage/: added to make available a manpage about idleconn.
  * debian/manpages: created to install the idleconn manpage.
  * debian/patches/:
      - 00list: removed. This file is obsolete.
      - 01_man_page_hyphens_patch.dpatch: removed. The upstream fixed
        the manpage. Thanks.
      - fix-manpage-formating:
          ~ Renamed to 10_fix_manpage.patch.
          ~ Added a header.
          ~ Added a new fix to a typo and to a spelling error.
      - SSLv23_client_method.diff:
          ~ Renamed to 20_SSLv23_client_method.patch.
          ~ Added a header.
  * debian/rules:
      - Added DEB_LDFLAGS_MAINT_APPEND variable to avoid some useless
        libraries dependencies.
      - Added DEB_BUILD_MAINT_OPTIONS to improve the GCC hardening.
  * debian/source:
      - Added information about new and old upstream repository.
      - Removed an old and obsolete information about patches in Debian.
  * debian/watch:
      - Bumped to version 4.
      - Added a fake site to explain about the current status of the
        original upstream homepage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 11 Mar 2016 11:17:20 -0300

httperf (0.9.0-3) unstable; urgency=medium

  * QA upload.
  * Use SSLv23_client_method instead of SSLv3_client_method (Closes: #804452)
  * Build with default Debian compiler flags.
  * Maintainer field set to QA Group.
  * Bump Standards-Version to 3.9.6.
  * Use dh_installman instead of dh_installmanpages.
  * Avoid starting synopsis with an article.
  * Add missing recommended debian/rules target: build-arch and build-indep.

 -- Emanuele Rocca <ema@debian.org>  Sat, 28 Nov 2015 18:30:31 +0100

httperf (0.9.0-2) unstable; urgency=low

  * Changing Maintainer Name
  * Adding Vcs-Headers to debian/control
  * Improve debian/copyright
  * Add ${misc:Depends} to depends
  * Add debian/README.source
  * Bump standards to 3.8.3 (no further changes needed)

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Mon, 18 Jan 2010 21:26:25 +0100

httperf (0.9.0-1) unstable; urgency=low

  * New maintainer (Closes: #483481)
  * New upstream release
  * Added watchfile
  * Moved to quilt
  * Transformed and updated  debian/patches-old/01_man_page_hyphens_patch.dpatch
    to debian/patches/fix-manpage-formating

 -- Alexander Schmehl <tolimar@debian.org>  Sat, 31 May 2008 17:17:52 +0200

httperf (0.8-8) unstable; urgency=low

  * Updated to Standards-Version 3.7.3 (no changes needed).
  * Updated to debhelper compatibility level 5.
  * Update maintainer email address.
  * debian/control: Switch Homepage to be a proper control field.
  * Make lintian happy with proper distclean call

 -- Roberto C. Sanchez <roberto@connexer.com>  Sat,  5 Jan 2008 11:32:00 -0500

httperf (0.8-7) unstable; urgency=low

  * Migrated all differences between upstream and the Debian package from
    the .diff.gz over to dpatch.
  * Ensure that differences in config.status and config.guess are no longer
    bloating the .diff.gz.

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Sat, 11 Mar 2006 11:49:41 -0500

httperf (0.8-6) unstable; urgency=low

  * Updated the URL in the package description.  Thanks to "The Anarcat" for
    pointing out the mistake. (Closes: #321264)

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Fri,  5 Aug 2005 17:12:53 -0400

httperf (0.8-5) unstable; urgency=low

  * Fix incorrect bug closure in changelog of version 0.8-2.  Thanks to Anibal
    Monsalve Salazar for pointing this out.

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Mon, 18 Jul 2005 19:10:20 -0400

httperf (0.8-4) unstable; urgency=low

  * Updated to Standards-Version 3.6.2.
  * Added license exception to copyright file.  The upstream maintainers and
    developers have provided an exception to the GPL that allows linking
    against OpenSSL.

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Tue, 12 Jul 2005 15:20:26 -0400

httperf (0.8-3) unstable; urgency=low

  * New version to force upload of .orig.tar.gz.  This is a result of the
    moving the package from non-US to main.

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Tue,  7 Jun 2005 23:38:27 -0400

httperf (0.8-2) unstable; urgency=low

  * New maintainer.
  * Moved from non-US to section web in main (Closes: #170060).
  * Build against new libssl in Sid (Closes: #215277).
  * Updated copyright file.
  * Added patch to correct lintian warnings on the man page.
  * Added a watch file.

 -- Roberto C. Sanchez <roberto@familiasanchez.net>  Tue,  7 Jun 2005 19:48:47 -0400

httperf (0.8-1) unstable; urgency=low

  * Initial Release (CLoses: #85914).

 -- Luca - De Whiskey's - De Vitis <luca@debian.org>  Sun, 22 Jul 2001 22:32:30 +0200

Local variables:
mode: debian-changelog
End:
